Event	ID	Name			Wwise Object Path	Notes
	47363338	ActivationOfPaint			\Default Work Unit\ActivationOfPaint	
	407893481	ColorPickUps			\Default Work Unit\ColorPickUps	
	862239991	BossSound			\Default Work Unit\BossSound	
	1551306167	UI			\Default Work Unit\UI	
	2266394309	KiddoFootsteps			\Default Work Unit\KiddoFootsteps	
	2368197235	EndLevelFeedback			\Default Work Unit\EndLevelFeedback	
	2491260467	PickUpLight			\Default Work Unit\PickUpLight	
	2999876861	TeleDoor			\Default Work Unit\TeleDoor	
	3128530969	KiddoLanding			\Default Work Unit\KiddoLanding	

Switch Group	ID	Name			Wwise Object Path	Notes
	1458816175	Footsteptype			\Default Work Unit\Footsteptype	
	1551306167	UI			\Default Work Unit\UI	
	2999876861	TeleDoor			\Default Work Unit\TeleDoor	

Switch	ID	Name	Switch Group			Notes
	1160234136	Normal	Footsteptype			
	1325827433	Blue	Footsteptype			
	3194545524	UISoundClick	UI			
	3637873365	IUStart	UI			
	3944003874	UISoundStartSelectLevel	UI			
	144862798	TeleDoorCloses	TeleDoor			
	1643113129	TeleDoorOpen	TeleDoor			
	2940176574	TeleDoorAppear	TeleDoor			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	70304652	Door closes	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Door closes_A1793BE9.wem		\Actor-Mixer Hierarchy\Pick-up\TeleDoor\TeleDoorCloses\Door closes		11205
	87377983	EndLevel	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\EndLevel_4865C65F.wem		\Actor-Mixer Hierarchy\Pick-up\EndLevelFeedback\EndLevel		54176
	138730444	Footstep2	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep2_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep2		2773
	153402499	Blue3	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue3_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue3		8620
	162210038	Blue8	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue8_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue8		9870
	224167816	Footstep7	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep7_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep7		2571
	259206472	Footstep1	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep1_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep1		3057
	306639096	Color Pick-ups	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Color Pick-ups_47626A1D.wem		\Actor-Mixer Hierarchy\Pick-up\Color Pick-ups\Color Pick-ups		38050
	324716178	Footstep3	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep3_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep3		2574
	347347293	Footstep8	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep8_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep8		2945
	350976423	Blue7	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue7_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue7		8045
	353399713	ActivationOfPaint	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\ActivationOfPaint_CE2198A7.wem		\Actor-Mixer Hierarchy\UI\ActivationOfPaint\ActivationOfPaint		4156
	373699020	The door is locked up	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\The door is locked up _CD951E02.wem		\Actor-Mixer Hierarchy\Pick-up\PickUpLight\The door is locked up		70147
	392154620	Footstep5	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep5_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep5		2677
	404412786	UISoundLevelSelect	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\UISoundLevelSelect_A1793BE9.wem		\Actor-Mixer Hierarchy\UI\UI\UISoundStartSelectLevel\UISoundLevelSelect		90575
	421424047	Blue6	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue6_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue6		6731
	500732272	Door Opens	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Door Opens_A1793BE9.wem		\Actor-Mixer Hierarchy\Pick-up\TeleDoor\TeleDoorOpen\Door Opens		11991
	602105996	Footstep4	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep4_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep4		2660
	672934117	Blue2	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue2_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue2		8424
	697905083	Footstep6	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep6_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep6		2637
	701541091	Boss sound	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Boss sound_DCEB8AE7.wem		\Actor-Mixer Hierarchy\Boss\BossSound\Boss sound		55443
	725132599	Blue1	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue1_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue1		7061
	753510757	Footstep9	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Footstep9_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Normal\Footstep9		2919
	764783104	UIClick	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\UIClick_B0178AB1.wem		\Actor-Mixer Hierarchy\UI\UI\UISoundClick\UIClick		11809
	788479451	(Start)Kiddo falder igennem et hul	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\(Start)Kiddo falder igennem et hul _D7879818.wem		\Actor-Mixer Hierarchy\UI\UI\Start\(Start)Kiddo falder igennem et hul		58531
	853681916	Blue4	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue4_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue4		8708
	979902690	Blue5	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Blue5_A1793BE9.wem		\Actor-Mixer Hierarchy\Kiddo\KiddoFootsteps\Blue\Blue5		9560
	1003840197	Door Appair	C:\Users\Dadiu student\Documents\DadiuGraduationGame\Kiddosfear\Kiddosfear_WwiseProject\.cache\Mac\SFX\Door Appair_A1793BE9.wem		\Actor-Mixer Hierarchy\Pick-up\TeleDoor\TeleDoorAppear\Door Appair		33269

