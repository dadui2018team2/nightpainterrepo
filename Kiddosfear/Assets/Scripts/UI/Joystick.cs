﻿using UnityEngine;
using UnityEngine.UI;

public class Joystick : MonoBehaviour {

    public Vector3 CurrentInput { get; private set; }

    [SerializeField] Image knobContainer;
    [SerializeField] Image knob;
    [SerializeField] float deadZone;
    [SerializeField] float maxDistanceFromDefault;
    [Tooltip("Should the joystick origin move to the initial touch position?")]
    [SerializeField] bool moveJoystick;

    private Vector2 defaultPosition;
    private Vector2 currentPosition;
    private bool joystickInUse;
    private int touchId;
 
    private void Awake() {
        defaultPosition = knobContainer.rectTransform.position + knobContainer.rectTransform.anchoredPosition3D;
        currentPosition = defaultPosition;
    }

    private void MoveKnob(Vector2 position) {
        Vector2 lerpedPos = Vector2.Lerp(Vector2.zero, (position.normalized * maxDistanceFromDefault) * (Screen.width / knob.canvas.scaleFactor), position.magnitude);
        Vector2 targetPos = knobContainer.transform.TransformPoint(lerpedPos);
        knob.rectTransform.position = targetPos;
    }

    public void OnJoystickDown() {
#if UNITY_ANDROID
        foreach (Touch touch in Input.touches) {
            if (touch.phase == TouchPhase.Began && !joystickInUse) {
                touchId = touch.fingerId;
                if (moveJoystick) {
                    currentPosition = touch.position;
                    transform.position = currentPosition;
                }
            }
        }
#endif
        if (Input.touchCount > 0){
            for (int i = 0; i < Input.touches.Length; i++){
                if (Input.touches[i].phase == TouchPhase.Began && !joystickInUse){
                    touchId = Input.touches[i].fingerId;
                    if (moveJoystick) {
                        currentPosition = Input.touches[i].position;
                        knobContainer.transform.position = currentPosition;
                    }
                }
            }
        }

        joystickInUse = true;
        if (moveJoystick){
            knobContainer.transform.position = currentPosition;
        }
        
        UpdateJoystick();
    }

    public void OnJoystickUp() {
        if (moveJoystick){
            knobContainer.transform.position = defaultPosition; 
        }
        joystickInUse = false;
        UpdateJoystick();
    }

    public void UpdateJoystick() {
        CurrentInput = GetInput();
        MoveKnob(new Vector2(CurrentInput.x, CurrentInput.z));
    }

    public Vector3 GetInput() {
        if (joystickInUse) {
#if UNITY_EDITOR || UNITY_STANDALONE
            Vector3 input;
            if (Input.touchCount > 0){
                input = GetTouchInput();
            }
            else {
                input = GetPCInput();
            }
            input.z = 0; //Remove this for 3D movement
            return input.normalized;

#elif UNITY_ANDROID
            return GetTouchInput();
#endif
        }

        return Vector3.zero;
    }

    private Vector3 GetTouchInput(){
        Touch touch = Input.GetTouch(GetFingerTouch(touchId));
        if (touch.phase == TouchPhase.Ended) {
            joystickInUse = false;
            return Vector3.zero;
        }
        Vector2 touchPos = knobContainer.transform.InverseTransformPoint(touch.position);
        Vector3 input = new Vector3(touchPos.x, 0, touchPos.y);
        if (touchPos.magnitude < deadZone * (Screen.width / knob.canvas.scaleFactor)) 
            input = Vector3.zero;

        input.z = 0; //Remove for 3D movement
        return input.normalized;
    }


    private Vector3 GetPCInput(){
        Vector2 centerPos = knobContainer.transform.position;
        Vector2 touchPos = knobContainer.transform.InverseTransformPoint(Input.mousePosition);
        Vector3 input = new Vector3(touchPos.x, 0, touchPos.y);

        if (input.magnitude < deadZone * (Screen.width / knob.canvas.scaleFactor))
            input = Vector3.zero;

        return input.normalized;
    }

    ///<summary>Returns the touch index for the given finger id.</summary>
	private int GetFingerTouch(int _fingerId){
		for (int i = 0; i < Input.touchCount; i++){
			Touch touch = Input.GetTouch(i);
			if (touch.fingerId == _fingerId) return i;
		}
		return 0;
	}
}
