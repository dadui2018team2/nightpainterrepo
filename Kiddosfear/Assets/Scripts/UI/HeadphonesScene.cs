﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class HeadphonesScene : MonoBehaviour {

    [Header("Canvas fading")]
    [SerializeField]
    Image blackFadeImage;
    [SerializeField]
    GameObject titleScreen;
    [SerializeField]
    float secondsBeforeAutoContinue;


    private void Awake()
    {
        //Checking audio settings
        if (!PlayerPrefs.HasKey("Music_Volume"))
            PlayerPrefs.SetFloat("Music_Volume", 100f);
        else {
            AkSoundEngine.SetRTPCValue("Music_Volume", PlayerPrefs.GetFloat("Music_Volume"));
        }

        if (!PlayerPrefs.HasKey("Sound_Volume"))
            PlayerPrefs.SetFloat("Sound_Volume", 100f);
        else {
            AkSoundEngine.SetRTPCValue("Sound_Volume", PlayerPrefs.GetFloat("Sound_Volume"));
        }

        //Fading in from black
        blackFadeImage.color = Color.black;
        StartCoroutine(FadeInFromBlack());

        StartCoroutine(LoadSceneAfterWait("MainMenu", secondsBeforeAutoContinue));
    }

    public void LoadAScene(string sceneName)
    {
        StopAllCoroutines();
        StartCoroutine(LoadSceneAfterWait(sceneName, 1f));
    }

    private IEnumerator LoadSceneAfterWait(string sceneName, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        blackFadeImage.gameObject.SetActive(true);
        float currentAlpha = blackFadeImage.color.a;
        float fadeValue = 0.01f;

        while (currentAlpha < 1f)
        {
            currentAlpha += fadeValue;
            currentAlpha = Mathf.Clamp(currentAlpha, 0f, 1f);

            Color col = blackFadeImage.color;
            col.a = currentAlpha;
            blackFadeImage.color = col;

            yield return new WaitForEndOfFrame();
        }

        titleScreen.SetActive(false);

        AsyncOperation newScene = SceneManager.LoadSceneAsync(sceneName);
    }

    
    #region Fading coroutines
    private IEnumerator FadeInFromBlack()
    {
        float currentAlpha = 1f;
        float fadeValue = 0.01f;

        while (currentAlpha > 0f)
        {
            currentAlpha -= fadeValue;
            currentAlpha = Mathf.Clamp(currentAlpha, 0f, 1f);

            Color col = blackFadeImage.color;
            col.a = currentAlpha;
            blackFadeImage.color = col;

            yield return new WaitForEndOfFrame();
        }

        blackFadeImage.gameObject.SetActive(false);
    }

    private IEnumerator FadeImage(Image img, float startAlpha, float endAlpha, bool setInactivateAtEnd)
    {
        float fadeValue = 0f;

        float currentAlpha = startAlpha;
        Color currentColor = img.color;
        currentColor.a = currentAlpha;

        while (currentAlpha != endAlpha)
        {
            fadeValue += Time.unscaledDeltaTime * 10f; //fading in half a second
            currentAlpha = Mathf.Lerp(startAlpha, endAlpha, fadeValue);
            currentColor.a = currentAlpha;

            img.color = currentColor;
            yield return new WaitForEndOfFrame();
        }
    }

    #endregion
}
