﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolTips : MonoBehaviour {
    public GameObject Tooltip;
    public GameObject TooltipBox;
	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Tooltip.gameObject.SetActive(true);
            TooltipBox.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
    }


    public void CloseWindow(GameObject go)
    {
        go.gameObject.SetActive(false);
    }
    public void OpenWindow(GameObject go)
    {
        go.gameObject.SetActive(true);
    }
}
