﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Takes a Linerenderer into the inspector both as LineRenderer and Gameobject(for disabling when not playing)

public class ShotCharge : MonoBehaviour {
    private float strenght = 0f;
    private float onestrenght = 0f;
    private float twostrenght = 0f;
    public LineRenderer lr;
    public LineRenderer olr;
    private bool charge = false;
    public GameObject LineRend;
    public Shooting shooting;
   

    public void ChargeShootYES()
    {
        charge = true;
        LineRend.SetActive(true);
    }

    public void ChargeShootNO()
    {
        charge = false;
        strenght = 0;
        onestrenght = 0;
        twostrenght = 0;

        Vector3[] positions = new Vector3[4];
        positions[0] = new Vector3(0f, 1f, 1f);
        positions[1] = new Vector3(0f, 1f, 1f);
        positions[2] = new Vector3(0f, 1f, 1f);
        positions[3] = new Vector3(0f, 1f, 1f);
        lr.positionCount = positions.Length;
        lr.SetPositions(positions);
        LineRend.SetActive(false);
    }
    void Start()
    {
      
    }
    void Update()
    {
        if (charge == true)
        {
            strenght += (2.5f*Time.deltaTime);
            onestrenght += (2.5f * Time.deltaTime);
            twostrenght += (2.5f * Time.deltaTime);
            Vector3[] positions = new Vector3[4];
            positions[0] = GameManager.Player.transform.position;
            positions[1] = shooting.shootDirection * onestrenght + GameManager.Player.transform.position;
            positions[2] = shooting.shootDirection * twostrenght + GameManager.Player.transform.position;
            positions[3] = shooting.shootDirection * strenght + GameManager.Player.transform.position;
            Vector3[] opositions = new Vector3[4];
            opositions[0] = GameManager.Player.transform.position;
            opositions[1] = GameManager.Player.transform.position;
            opositions[2] = GameManager.Player.transform.position;
            opositions[3] = shooting.shootDirection * 7f + GameManager.Player.transform.position;
            lr.positionCount = positions.Length;
            lr.SetPositions(positions);
            olr.positionCount = positions.Length;
            olr.SetPositions(opositions);

            //Checks that the values for each segments end position doesnt 
             if (strenght >= 8f)
             {
                strenght = 0;
                onestrenght = 0;
                twostrenght = 0;
                charge = false;
                LineRend.SetActive(false);
             }
            if (onestrenght >= 2.5f)
            {
                onestrenght = 2.5f;
            }
            if (twostrenght >= 4.5f)
            {
                twostrenght = 4.5f;
            }
        }
    }

       
    
}
