﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {

    [SerializeField] Joystick joystick;
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] GameObject projectilePool;

    [Header("Materials to use")]
    [SerializeField] Material unColored;
    [SerializeField] Material red;
    [SerializeField] Material green;
    [SerializeField] Material blue;

    [Header("Settings")]
    public float maxShootingDistance;
    [SerializeField] float shotMagnitudeIncrease;
    [SerializeField] int maxActiveProjectiles;

    private Transform mainCamera;
    
    public Vector3 shootDirection;
    public float currentShotMagnitude = 0f;
    public bool hasShot;
    private int nextProjectile;

    private Material materialToShoot;
    [HideInInspector] public string colorToShoot = "";

    private List<GameObject> projectiles = new List<GameObject>();


    private int layerMask = 1 << 9;

    public bool isChargingUpSound;

    private void Awake() {
        mainCamera = Camera.main.transform;

        for (int i = 0; i < maxActiveProjectiles; i++) {
            GameObject projectile = Instantiate(projectilePrefab, projectilePool.transform, false);
            projectile.SetActive(false);
            projectiles.Add(projectile);
        }
        nextProjectile = 0;
        isChargingUpSound = false;

        //Automatically picking the color that was previously picked
        if (!PlayerPrefs.HasKey("CurrentColor"))
            PlayerPrefs.SetString("CurrentColor", "");

        SelectColor(PlayerPrefs.GetString("CurrentColor"));
    }

    private void FixedUpdate() {
        Quaternion rotation = new Quaternion(0f, mainCamera.rotation.y, 0f, mainCamera.rotation.w);
        shootDirection = rotation * joystick.CurrentInput;

        if (shootDirection.magnitude > 0 && colorToShoot != "") {
            if (!isChargingUpSound) {
                uint bankLoad;
                AkSoundEngine.LoadBank("KiddoShootsPaint", -1, out bankLoad);
                AkSoundEngine.PostEvent("KiddoShootsPaint", gameObject);
                isChargingUpSound = true;
            }
            currentShotMagnitude += shotMagnitudeIncrease;
            AkSoundEngine.SetRTPCValue("KiddoShootsPaintChargingUp_RTCP", currentShotMagnitude);
            if (currentShotMagnitude > maxShootingDistance) {
                joystick.OnJoystickUp();
                Shoot();
            }
        }

        Debug.DrawLine(transform.position, transform.position + (shootDirection * currentShotMagnitude), Color.magenta);
    }

    /// <summary>
    /// Setting 'hasShot' to false to only shoot once per joystick-interaction
    /// </summary>
    public void PrepareShot() {
        hasShot = false;
    }

    /// <summary>
    /// Initializing the next free projectile in the projectile pool and resetting the shooting magnitude.
    /// </summary>
    public void Shoot() {
        if (hasShot == true || colorToShoot == "") {
            currentShotMagnitude = 0f;
            return;
        }

        if (projectiles[nextProjectile].activeInHierarchy == false) { //if the next projectile to shoot is not currently in use
            projectiles[nextProjectile].SetActive(true);
            projectiles[nextProjectile].GetComponent<Projectile>().InitializeProjectile(transform.position, transform.position + (shootDirection * currentShotMagnitude), colorToShoot, materialToShoot);

            AkSoundEngine.PostEvent("ColorFlyingThroughAir", projectiles[nextProjectile]);

            //Increasing the projectile index and wrapping it to the start of the list
            nextProjectile++;
            if (nextProjectile == projectiles.Count)
                nextProjectile = 0;
        }

        currentShotMagnitude = 0f;
        hasShot = true;

        int bankUnload;
        AkSoundEngine.UnloadBank("KiddoShootsPaint", (System.IntPtr)0, out bankUnload);
        isChargingUpSound = false;

    }

    /// <summary>
    /// Selecting the color/material to put on cubes and projectiles via a string
    /// </summary>
    /// <param name="color">Red, Green or Blue</param>
    public void SelectColor(string color) {
        AudioPlayer.PlayEvent("ActivationOfPaint", gameObject, 3f);

        switch (color) {
            case "Red":
                colorToShoot = color;
                materialToShoot = red;
                PlayerPrefs.SetString("CurrentColor", color);
                break;
            case "Green":
                colorToShoot = color;
                materialToShoot = green;
                PlayerPrefs.SetString("CurrentColor", color);
                break;
            case "Blue":
                colorToShoot = color;
                materialToShoot = blue;
                PlayerPrefs.SetString("CurrentColor", color);
                break;
            case "":
                break;
            default:
                Debug.LogError("Attempted to pick invalid color: " + color + ". Color must either be Red, Green or Blue, case sensitive.");
                break;
        }
    }
}
