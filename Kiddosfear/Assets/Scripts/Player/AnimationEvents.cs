﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimationEvents : MonoBehaviour {

    ActionAnimator actionAnimator;
    CameraAnimation cameraAnimation;

    private void Awake() {
        actionAnimator = FindObjectOfType<ActionAnimator>();
        cameraAnimation = FindObjectOfType<CameraAnimation>();
    }

    public void PlayNormalFootStep() {
        if (actionAnimator.LocomotionLayerHidden())
            return;

        AkSoundEngine.PostEvent("KiddoFootsteps", gameObject);
    }

    public void PlaySpecialFootStep() {
        AkSoundEngine.PostEvent("KiddoFootsteps", gameObject);
    }

    public void PlayLanding() {
        AkSoundEngine.PostEvent("KiddoFootsteps", gameObject);
    }

    public void EndAnimation(string nextScene){
        SceneManager.LoadSceneAsync(nextScene);
    }

    public void SkipAnimation(){
        BlackFadeIn(0.05f);
        Invoke("GoToLevelOne", 1f);
    }

    private void GoToLevelOne(){
        EndAnimation("Level1_2D");
    }

    public void BlackFadeOut(float step){
        cameraAnimation.BeginFade(1, 0, step);
    }

    public void BlackFadeIn(float step){
        cameraAnimation.BeginFade(0, 1, step);
    }
}
