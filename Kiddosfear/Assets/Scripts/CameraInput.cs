﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInput : MonoBehaviour {
	/*
	Script for sending input to CameraController. Should have a CameraController component on the MainCamera for this to work.
	*/

	private CameraController _cameraController;

	private List<int> fingersActive;
	private Vector2 move;
	private float zoomDelta;

	private void Awake() {
		fingersActive = new List<int>();
		_cameraController = Camera.main.GetComponent<CameraController>();
	}

	///<summary> Adds new finger to the active finger list.</summary>
	public void OnFingerDown(){
		for (int i = 0; i < Input.touches.Length; i++) {
            if (Input.touches[i].phase == TouchPhase.Began) {
				fingersActive.Add(Input.touches[i].fingerId);
            }
        }
	}

	///<summary>Updates the current list of active fingers by removing any inactive fingers.</summary>
	public void UpdateActiveFingers(){
		if (fingersActive.Count >= 1) {
			for (int i = 0; i < fingersActive.Count; i++){
				Touch touch = Input.GetTouch(GetFingerTouch(fingersActive[0]));
				if (touch.phase == TouchPhase.Ended) {
					fingersActive.RemoveAt(i);
					UpdateActiveFingers(); // Repeat until there's no more ended.
				}
			}
		}
	}

	///<summary>Returns the touch index for the given finger id.</summary>
	private int GetFingerTouch(int _fingerId){
		for (int i = 0; i < Input.touchCount; i++){
			Touch touch = Input.GetTouch(i);
			if (touch.fingerId == _fingerId) return i;
		}
		return 0;
	}

	private void Update() {
		UpdateActiveFingers();

		if (fingersActive.Count == 1){
			// Move camera
			move = Input.GetTouch(0).deltaPosition;
		}
		else if (fingersActive.Count == 2){
			// Zoom camera
			Touch touch0 = Input.GetTouch(GetFingerTouch(fingersActive[0]));
			Touch touch1 = Input.GetTouch(GetFingerTouch(fingersActive[1]));

			Vector2 input0 = touch0.deltaPosition;
			Vector2 pos0 = touch0.position;

			Vector2 input1 = touch1.deltaPosition;
			Vector2 pos1 = touch1.position;

			float prevDistance = Vector2.Distance(pos0, pos1);
			float newDistance = Vector2.Distance(input0 + pos0, input1 + pos1);

			zoomDelta = newDistance - prevDistance;
		}
	}

	private void LateUpdate() {
		if (fingersActive.Count == 1){
			// Move camera
			//_cameraController.Move(move.x, -move.y);
		}
		else if (fingersActive.Count == 2){
			// Zoom camera
			//_cameraController.Zoom(zoomDelta);
		}
	}
}
