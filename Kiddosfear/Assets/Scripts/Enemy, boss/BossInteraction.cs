﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossInteraction : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Boss") {
            other.gameObject.GetComponent<BossBehaviour>().LoseHealth();
        }
    }
}
