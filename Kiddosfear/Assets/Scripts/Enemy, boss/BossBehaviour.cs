﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BossBehaviour : MonoBehaviour {

    // Script for controlling boss health and it's transition phases

    #region Boss stats
    [Header("Boss stats")]
    [Tooltip("Reference to the healthbar slider")]
    public Slider healthSlider;
    [Tooltip("Movement speed of the boss")]
    public float bossMoveSpeed;

    private int bossStartingHealth = 21;
    private int bossCurrentHealth;
    private int smallDmgChunk = 1;
    private int largeDmgChunk = 5;
    private int hitCounter = 0;
    #endregion

    #region Transition and animation

    [Header("Transition and animation")]
    private Animator anim;
    [Tooltip("Time it takes for the boss to fade into invisibility")]
    public float fadeDuration;
    [Tooltip("Time the boss is invisible")]
    public float invisDuration;
    [Tooltip("Time it takes the boss to spawn after a new spawnpoint has been determined")]
    public float spawnDuration;
    [Tooltip("Time it takes the animation on the healthslider to finish")]
    public float healthAnimDuration; //Note: The player cannot hit the boss while it's health is being lowered
    [Space]
    [Tooltip("The radius around the player where the boss can spawn after transition phase")]
    public float spawnRadius;

    private int phase = 0;
    private float spawnMinimizer = 1;
    private float size = 1;
    private bool shouldFade;

    private Vector3 spawnPoint;
    private Renderer render;
    private SkinnedMeshRenderer meshRend;
    private Vector3 initPos;
    #endregion

    #region script control
    private bool canLoseHealth = true;
    private bool canMove = true;
    private bool shouldStartTransition = true;
    private bool shouldStartHealthAnim = true;
    #endregion

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        render = GetComponentInChildren<Renderer>();
        meshRend = GetComponentInChildren<SkinnedMeshRenderer>();
        
        bossCurrentHealth = bossStartingHealth;

        initPos = transform.position;

        AudioPlayer.LoadBank("BossSound");
        //PlayEvent("BossSound", gameObject);

        //AudioPlayer.PlayEventWithSwitch("Boss", "Boss", "BossSpawning", gameObject, 3f);
        AudioPlayer.SetSwitch("Boss", "BossSpawning", gameObject);
        AkSoundEngine.PostEvent("BossSound", gameObject);
    }
	
	void Update () {
        if (canMove)
            FollowPlayer();

        //draws ray with the same radius as the spawn radius. Makes it easier for gamedesigner/leveldesigner to determine spawn radius
        Debug.DrawRay(GameManager.Player.transform.position, Vector3.left * spawnRadius, Color.yellow); 
    }

    /// <summary>
    /// Controls when the boss can lose health and how much health he should loses upon hit
    /// </summary>
    public void LoseHealth() {
        if (canLoseHealth) {
            hitCounter++;
            canLoseHealth = false;

            if (hitCounter % 3 == 0) { //Check for every third hit
                canMove = false;
                anim.SetTrigger("bigHit");

                AudioPlayer.SetSwitch("Boss", "BossTakingHeavyDamage", gameObject);
                AkSoundEngine.PostEvent("Boss", gameObject);

                if (shouldStartHealthAnim) {
                    bossCurrentHealth -= largeDmgChunk;
                    StartCoroutine(AnimateHealthSlider(largeDmgChunk));
                }
                
                if (shouldStartTransition) {
                    StartCoroutine(ActivateTransitionPhase());
                }
            }
            else {
                anim.SetTrigger("smallHit");
                AudioPlayer.SetSwitch("Boss", "BossTakingDamage", gameObject);
                AkSoundEngine.PostEvent("Boss", gameObject);
                if (shouldStartHealthAnim) {
                    bossCurrentHealth -= smallDmgChunk;
                    StartCoroutine(AnimateHealthSlider(smallDmgChunk));
                }
            }
        }
    }

    /// <summary>
    /// Handles how the boss behaves during transition phase
    /// </summary>
    /// <returns></returns>
    IEnumerator ActivateTransitionPhase() {
        shouldStartTransition = false;
        AudioPlayer.SetSwitch("Boss", "BossStartingToFade", gameObject);
        AkSoundEngine.PostEvent("Boss", gameObject);

        //Disables collider during invisibility
        gameObject.GetComponent<Collider>().enabled = false;
        gameObject.GetComponentInChildren<Collider>().enabled = false;

        //Wait for the boss to finish the hit animation
        while (HasFinishedAnim("bigHit") == false)
            yield return new WaitForEndOfFrame();
        

        //Disable Renderer to make the boss invisible
        render.enabled = false;
        
        yield return new WaitForSeconds(invisDuration);
        
        DetermineSpawnPoint(spawnRadius);

        if(phase == 0)
            meshRend.SetBlendShapeWeight(phase, 100);
        else if (phase == 1) {
            meshRend.SetBlendShapeWeight(phase - 1, 0);
            meshRend.SetBlendShapeWeight(phase, 100);
        }

        //Place a cube at the determined spawnpoint to warn the player that the boss is spawning here. Should be changed to something more appropriate later
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = new Vector3(spawnPoint.x, spawnPoint.y - transform.localScale.y, spawnPoint.z);
        yield return new WaitForSeconds(spawnDuration);

        //Move boss to dermined spawn point
        transform.position = spawnPoint;
        AudioPlayer.SetSwitch("Boss", "BossStartingToSpawnAgain", gameObject);
        AkSoundEngine.PostEvent("Boss", gameObject);

        //re-enable collider and enable the renderer again so the player can see the player
        anim.SetTrigger("spawnBoss");
        yield return new WaitForSeconds(0.3f);
        gameObject.GetComponent<Collider>().enabled = true;
        render.enabled = true;

        phase++;
        anim.SetInteger("Stage", phase);
        canLoseHealth = true;
        canMove = true;
        shouldStartTransition = true;
        yield break;
    }

    /// <summary>
    /// Animates the healthslider depending on the damage from a given hit
    /// </summary>
    /// <param name="dmgVal"></param>
    /// <returns></returns>
    IEnumerator AnimateHealthSlider(float dmgVal) {
        shouldStartHealthAnim = false;

        //Set variables to use for animating the health slider
        float lerpStartTime = Time.time;
        float lerpStartValue = healthSlider.value;
        float lerpEndValue = healthSlider.value - dmgVal;
        float lerpProgress;
        int counter = 0;

        bool shouldLerp = true;
        while (shouldLerp && counter < 300) { //Lerps the healthslider to animate losing health. The duration of the lerp depends on the healthAnimDuration variable
            yield return new WaitForEndOfFrame();

            lerpProgress = Time.time - lerpStartTime;
            if (healthSlider != null)
                healthSlider.value = Mathf.Lerp(lerpStartValue, lerpEndValue, lerpProgress / healthAnimDuration);
            else
                shouldLerp = false;

            if (lerpProgress >= healthAnimDuration)
                shouldLerp = false;

            counter++;
        }

        //if the boss's health reaches 0 change scene to next scene
        if (bossCurrentHealth <= 0) {
            AudioPlayer.UnloadBank("BossSound", 2f);
            healthSlider.fillRect.gameObject.SetActive(false);
            SceneManager.LoadScene("EndGameScene");
        }

        canLoseHealth = true;
        shouldStartHealthAnim = true;
    }

    /// <summary>
    /// Determines a valid spawnpoint for the boss to spawn. The spawnpoint is a radius araound the player
    /// </summary>
    /// <param name="currentSpawnRad"></param>
    private void DetermineSpawnPoint(float currentSpawnRad) {

        //Find a random point around the player
        spawnPoint = GameManager.Player.transform.position;
        spawnPoint.x += Random.Range(-currentSpawnRad, currentSpawnRad);
        spawnPoint.z += Random.Range(-currentSpawnRad, currentSpawnRad);

        //Cast a ray downwards to find a valid spawnpoint on the floor
        RaycastHit hit;
        if (Physics.Raycast(spawnPoint, Vector3.down, out hit, 10)) {
            Debug.DrawRay(spawnPoint, Vector3.down * 10, Color.green);
            spawnPoint = hit.point;
        }
        else {
            //if no valid spawnpoint was found reduce the radius and try again
            currentSpawnRad =- spawnMinimizer;
            if(currentSpawnRad <= 0) { //If no valid spawnpoint was found after several tries of reducing the search space, spawn the boss at the players location
                Debug.Log("Couldn't find new spawnpoint");
                spawnPoint = GameManager.Player.transform.position;
            }
            else {
                DetermineSpawnPoint(currentSpawnRad);
            }
            
        }
        
    }

    /// <summary>
    /// Moves and rotates the boss towards the player
    /// </summary>
   private void FollowPlayer() {
        //Move player towards player. Makes sure the boss doesn't float
        Vector3 newPos = (GameManager.Player.transform.position - transform.position).normalized * bossMoveSpeed * Time.deltaTime;
        transform.position += new Vector3(newPos.x, 0, newPos.z);

        //Rotates the boss towards the player. Makes sure the boss doesn't tilt
        Vector3 playerPos = GameManager.Player.transform.position;
        playerPos.y = transform.position.y;
        Vector3 lookDir = Vector3.RotateTowards(transform.forward, playerPos - transform.position, 180 * Time.deltaTime, 0);
        transform.rotation = Quaternion.LookRotation(lookDir);
        
    }

    /// <summary>
    /// Determines whether an animation is done playing
    /// </summary>
    /// <param name="animName">Name of the animation clip</param>
    /// <returns></returns>
    private bool HasFinishedAnim(string animName) {
        if (anim.GetBool(animName) == false && anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.95f)
            return true;
        else
            return false;
    }

    
}
