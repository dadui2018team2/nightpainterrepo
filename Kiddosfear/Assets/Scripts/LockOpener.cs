﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOpener : MonoBehaviour {

    public InGameDoors exitDoor;

    private void Start() {
        exitDoor = transform.parent.GetComponent<InGameDoors>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            if (exitDoor.CheckDoor()) {
                exitDoor.UnlockDoor();
            }
        }
    }

}
