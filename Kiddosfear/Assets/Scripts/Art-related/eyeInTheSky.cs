﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eyeInTheSky : MonoBehaviour {

    public bool lookAtCamera = true;

	void Start () {
    
        // Getting the animation to start at a differnt time 
        Animator anim = GetComponent<Animator>();
        AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);
        anim.Play(state.fullPathHash, -1, Random.Range(0f, 1f));
    }
	
	void Update () {

        // Rotating the object towards the camera
        if (lookAtCamera)
            transform.LookAt(Camera.main.transform.position, Vector3.up);
    }
}
