﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour {

    [Range(0f,1f)]
    [SerializeField] float minimumIntensity;

    [Range(0f,1f)]
    [SerializeField] float maximumIntensity;

    [SerializeField] float intensityChangePerFrame;

    private Light lightComponent;
    private float smoothStepValue = 0f;
    private bool reverse = false;

    private void Awake() {
        lightComponent = GetComponent<Light>();
        lightComponent.intensity = minimumIntensity;
    }

    private void Update() {
        float newIntensity;
        smoothStepValue += intensityChangePerFrame;
        smoothStepValue = Mathf.Clamp(smoothStepValue, 0f, 1f);

        if (reverse) {
            newIntensity = Mathf.SmoothStep(maximumIntensity, minimumIntensity, smoothStepValue);
            lightComponent.intensity = newIntensity;

            if (newIntensity == minimumIntensity) {
                reverse = false;
                smoothStepValue = 0f;
            }
        }
        else {
            newIntensity = Mathf.SmoothStep(minimumIntensity, maximumIntensity, smoothStepValue);
            lightComponent.intensity = newIntensity;

            if (newIntensity == maximumIntensity) {
                reverse = true;
                smoothStepValue = 0f;
            }
        }
    }
}
