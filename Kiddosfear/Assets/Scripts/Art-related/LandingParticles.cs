﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingParticles : MonoBehaviour {

    private bool hasPlayed;

    private void Start() {
        if (MainMenuSettings.previousScene != "MainMenu")
            gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && !hasPlayed) {
            hasPlayed = true;
            StartCoroutine(PlayParticles());
        }
    }

    IEnumerator PlayParticles() {
        ParticleSystem particles = GetComponentInChildren<ParticleSystem>();

        particles.Play();
        AudioPlayer.PlayEvent("PickUpLight", gameObject, 3);

        while (particles.isPlaying) {
            yield return new WaitForEndOfFrame();
        }

        particles.Stop();
        gameObject.SetActive(false);
    }
}
