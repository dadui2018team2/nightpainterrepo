﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {

    public Animation anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animation>();
	}

    private void Update() {
        if (!anim.IsPlaying("credits")) {
            LoadScene("MainMenu");
        }

    }

    public void LoadScene(string scene) {
        SceneManager.LoadScene(scene);
    }
}
