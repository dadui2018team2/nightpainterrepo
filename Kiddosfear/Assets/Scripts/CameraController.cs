﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	/*
	Script for moving camera around
	 */

	[Header("Focus points")]
	[Tooltip("Player point to focus when zoomed in.")]
	public GameObject _player;
	[Tooltip("World point to focus when zoomed out.")]
	public GameObject _world;
	[Range(0f, 1f)]
	[Tooltip("The threshold of switching between the two focus points when zooming. 0 = Player, 1 = World.")]
	public float _threshold = 0.5f;
	[Range(0.01f, 1f)]
	[Tooltip("Smoothening for thresholding between the two points.")]
	public float _thresholdSmoothening = 0.5f;
	[Tooltip("Should the camera lerp between world and player, instead of thresholding?")]
	public bool _lerpBetweenPoints = false;

    [HideInInspector]
    public bool shouldFollow = true;

	[Header("Zoom")]
	public float _maxCameraDistance = 10f;
	public float _minCameraDistance = 2f;
	public float _zoomSpeed = 1f;
	[Range(0.01f, 1f)]
	[Tooltip("How smooth is the camera movement? 1 = instant, down to 0.01.\nAffects zoom and focus switch.")]
	public float _zoomSmoothening = 0.5f;

	[Header("Debug")]
	[Tooltip("Allow use of mouse scrollwheel to zoom, and arrows/WASD to rotate camera.")]
	public bool allowMouseAndKeyboard = false;

	private float zoomDistance;
	private Vector3 cameraRotation;
	private GameObject focusPoint;
	[HideInInspector] public Vector3 focusPointPosition;

	#region input
	private List<int> fingersActive;
	private Vector2 rotationDirection;
	private float zoomDelta;
	#endregion

	private Vector3 playerLastPos;

	private void Awake() {
		if (_player == null) _player = gameObject;
		if (_world == null) _world = gameObject;
		cameraRotation = transform.forward;
		
		focusPoint = _player;
		focusPointPosition = focusPoint.transform.position;
		playerLastPos = _player.transform.position;

		fingersActive = new List<int>();

		/*
		Move(transform.rotation.eulerAngles.y,transform.rotation.eulerAngles.x);
		zoomDistance = Vector3.Distance(transform.position, focusPoint.transform.position);
		Zoom(0);
		 */
	}

	private void Start() {
		zoomDistance = Mathf.Clamp(Vector3.Distance(transform.position, GameManager.Player.transform.position), _minCameraDistance, _maxCameraDistance);
	}
	
	private void LateUpdate () {
		zoomDelta = 0;

		if (allowMouseAndKeyboard) PCInput();

		UpdateActiveFingers();
		if (fingersActive.Count == 2){
			// Zoom camera
			Touch touch0 = Input.GetTouch(GetFingerTouch(fingersActive[0]));
			Touch touch1 = Input.GetTouch(GetFingerTouch(fingersActive[1]));

			Vector2 input0 = touch0.deltaPosition;
			Vector2 pos0 = touch0.position;

			Vector2 input1 = touch1.deltaPosition;
			Vector2 pos1 = touch1.position;

			float prevDistance = Vector2.Distance(pos0, pos1);
			float newDistance = Vector2.Distance(input0 + pos0, input1 + pos1);

			zoomDelta = newDistance - prevDistance;
		}

		CheckFocusPoint();
		if (GameManager.CurrentGameState == GameManager.GameState.Play || GameManager.CurrentGameState == GameManager.GameState.ShowLevelName) {
			Zoom(zoomDelta);
		}
	}

	/// <summary>Checks which focus point to choose based on zoom level and threshold.</summary>
	private void CheckFocusPoint(){
        if (!shouldFollow)
            return;

		if (Mathf.Lerp(_minCameraDistance, _maxCameraDistance, _threshold) < zoomDistance) focusPoint = _world;
		else focusPoint = _player; 
		
		float lerpValue = _zoomSmoothening;
		if (_lerpBetweenPoints){
			lerpValue = Mathf.InverseLerp(_minCameraDistance, _maxCameraDistance, zoomDistance);
			Vector3 newPos = Vector3.Lerp(_player.transform.position, _world.transform.position, lerpValue);
			focusPointPosition = newPos;
		} 
		else {
			Vector3 newPos = Vector3.Lerp(focusPointPosition, focusPoint.transform.position, _thresholdSmoothening);
			focusPointPosition = newPos;
		}
	}    

	/// <summary>Increase/decrease distance between camera and focus point.</summary>
	private void Zoom(float _z){
		zoomDistance -= _z * _zoomSpeed;
		if (zoomDistance < _minCameraDistance) zoomDistance = _minCameraDistance;
		else if (zoomDistance > _maxCameraDistance) zoomDistance = _maxCameraDistance;

		Vector3 newPos = focusPointPosition - transform.TransformDirection(Vector3.forward * zoomDistance);
		transform.position = Vector3.Lerp(transform.position, newPos, _zoomSmoothening);
	}

	///<summary> Adds new finger to the active finger list.</summary>
	public void OnFingerDown(){
		for (int i = 0; i < Input.touches.Length; i++) {
            if (Input.touches[i].phase == TouchPhase.Began) {
				fingersActive.Add(Input.touches[i].fingerId);
            }
        }
	}

	///<summary>Updates the current list of active fingers by removing any inactive fingers.</summary>
	private void UpdateActiveFingers(){
		if (fingersActive.Count >= 1) {
			for (int i = 0; i < fingersActive.Count; i++){
				Touch touch = Input.GetTouch(GetFingerTouch(fingersActive[0]));
				if (touch.phase == TouchPhase.Ended) {
					fingersActive.RemoveAt(i);
					UpdateActiveFingers(); // Repeat until there's no more ended.
				}
			}
		}
	}

	///<summary>Returns the touch index for the given finger id.</summary>
	private int GetFingerTouch(int _fingerId){
		for (int i = 0; i < Input.touchCount; i++){
			Touch touch = Input.GetTouch(i);
			if (touch.fingerId == _fingerId) return i;
		}
		return 0;
	}

	private void PCInput(){
		rotationDirection += new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * 5;
		zoomDelta += Input.mouseScrollDelta.y * 2;
	}
}
