﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDoorInteraction : MonoBehaviour {

    public int doorLevel;
    public bool canLoadLevel = false;
    public float minimumDistanceToPlayer;

    private Animator animator;
    private GameObject player;
    private float timeWaited = 0f;

    private Coroutine levelLoadSequence;

    private void Awake() {
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player");

        
    }

    private void Start() {
        if (MainMenuSettings.previousScene != string.Format("Level{0}_2D", doorLevel.ToString())) {
            canLoadLevel = true;
        }
    }

    public void Update() {
        if (!canLoadLevel) {
            timeWaited += Time.deltaTime;

            if (timeWaited < 1f) {
                timeWaited += Time.deltaTime;
            }
            else {
                //Check if player has run far enough away to be able to load a level
                if (Vector3.Distance(transform.position, player.transform.position) > minimumDistanceToPlayer) {
                    canLoadLevel = true;
                }
            }
        }
    }

    /// <summary>
    /// Activates and opens the door.
    /// </summary>
    public void ActivateAndOpenDoor() {
        ActivateDoor();
        Invoke("OpenDoor", 1f);
    }

    /// <summary>
    /// Activates the door.
    /// </summary>
    public void ActivateDoor() {
        animator.Play("DoorActivate");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Opens the door (Should only be called after activating!)
    /// </summary>
    public void OpenDoor() {
        animator.Play("OpenDoor");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorOpen", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player" && canLoadLevel) {
            if(levelLoadSequence == null)
                levelLoadSequence = StartCoroutine(ExitLevel());
        }
    }

    private IEnumerator ExitLevel() {
        //Get references
        PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();
        Animator playerAnimator = player.GetComponent<Animator>();
        CameraAnimation camAnim = FindObjectOfType<CameraAnimation>();
        CameraController camController = camAnim.gameObject.GetComponent<CameraController>();

        //Remove GUI and stop camera follow
        FindObjectOfType<UIGameManager>().ShowGUI(false);
        camController.enabled = false;

        //Stop player movement + painting
        playerMovement.enabled = false;
        playerMovement.acceptUserInput = false;
        playerMovement.moveLeft = false;
        playerMovement.moveRight = false;
        player.GetComponentInChildren<TouchPaint>().enabled = false;

        yield return new WaitForEndOfFrame();

        //Make Kiddo walk through door
        playerMovement.walkThroughDoor = true;
        playerMovement.acceptUserInput = true;
        playerMovement.enabled = true;
        playerAnimator.Play("run_cleaned", 0);
        while (player.transform.position.z < 7f) {
            yield return new WaitForEndOfFrame();
        }
        playerMovement.enabled = false;
        player.SetActive(false);

        //Close door
        animator.Play("DoorDeactivate");
        yield return new WaitForSeconds(1f);
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorCloses", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);

        //Wait a couple of seconds before going to the next scene
        camAnim.BeginFade(0f, 1f, 0.01f);

        while (camAnim.isFading)
            yield return new WaitForEndOfFrame();

        FindObjectOfType<WwiseMusicController>().OnDestroy();

        //Load the next scene
        FindObjectOfType<UIGameManager>().LoadScene(string.Format("Level{0}_2D", doorLevel.ToString()));






        /*
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 3f);

        player.walkThroughDoor = true;
        while (Vector3.Distance(player.transform.position, transform.position) > 2f) {
            yield return new WaitForEndOfFrame();
        }

        UIGameManager uiGameManager = FindObjectOfType<UIGameManager>();
        //if(doorLevel == 1) {
        //    uiGameManager.LoadScene("IntroAnimation");
        //}
        //else
            uiGameManager.LoadScene(string.Format("Level{0}_2D", doorLevel.ToString()));

    */
    }

}

    /*[Header("Updated in awake")]
    public LevelDoorManager levelDoorManager;
    public Animator animator;

    [Header("Door variables (do not touch)")]
    public bool isActive;
    public bool isOpen;
    public GameObject otherDoor;
    


    private void Awake() {
        levelDoorManager = transform.parent.GetComponent<LevelDoorManager>();
        animator = GetComponent<Animator>();

    }

    /// <summary>
    /// Checks if the current door is active. If it isn't, it will activate.
    /// </summary>
    public void AttemptActivateDoor() {
        if (!isActive) {
            if (levelDoorManager.DoorIsNewCheckpoint(gameObject)) {
                levelDoorManager.ActivateDoors(this);
                int doorValue = (int)char.GetNumericValue(gameObject.name[gameObject.name.Length - 1]);
                levelDoorManager.SaveDoorCheckpoint(doorValue);
            }
        }
    }
    
    

    /// <summary>
    /// Closes the door.
    /// </summary>
    public void CloseDoor() {
        animator.Play("CloseDoor");
        isOpen = false;
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorClose", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Sets which door to teleport to, when entering the door.
    /// </summary>
    /// <param name="door"></param>
    public void SetOtherDoor(GameObject door) {
        otherDoor = door;
    }

    

    /// <summary>
    /// Invokes private or public function in the script.
    /// </summary>
    /// <param name="functionName">Name of the function to run</param>
    /// <param name="time">Delay before running the function</param>
    public void InvokeClassFunction(string functionName, float time) {
        Invoke(functionName, time);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            DoorInteraction otherDoorInteraction = otherDoor.GetComponent<DoorInteraction>();

            if (otherDoor == null) Debug.Log("Can't find 'otherDoor' in DoorInteraction.cs. Current door: " + gameObject.name);

            otherDoorInteraction.OpenDoor();
            other.transform.position = otherDoorInteraction.gameObject.transform.position + (otherDoorInteraction.gameObject.transform.forward * 2);
            AudioPlayer.SetSwitch("TeleDoor", "TeleDoorKiddoWalksThrough", otherDoor.gameObject);
            AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
            otherDoorInteraction.Invoke("CloseDoor", 1f);
        }
    }
}



    */