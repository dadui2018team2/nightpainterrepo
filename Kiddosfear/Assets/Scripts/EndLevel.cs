﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class EndLevel : MonoBehaviour {

    /*
	This script is for ending the level when player enters this object's trigger. Works in cooperation with GameManager (EndLevel()).
	 */

    public GameObject startPanPos;
    private GameObject player;
    private ActionAnimator actionAnimator;

    private Coroutine endLevelSequence;

    private void Awake() {
        actionAnimator = FindObjectOfType<ActionAnimator>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            player = other.gameObject;

            if (endLevelSequence == null)
                endLevelSequence = StartCoroutine(EndLevelSequence());
        }
	}

    private IEnumerator EndLevelSequence() {
        //Save progress
        int completedLevel = GetCurrentLevelNumber();
        if (PlayerPrefs.HasKey("CompletedLevels")) {
            if (completedLevel > PlayerPrefs.GetInt("CompletedLevels"))
                PlayerPrefs.SetInt("CompletedLevels", completedLevel);
        }
        else {
            PlayerPrefs.SetInt("CompletedLevels", completedLevel);
        }
       

        //Get references
        PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();
        GameObject endLevelParticles = player.transform.Find("VisualEffects_LevelEnd").gameObject;
        CameraAnimation camAnim = FindObjectOfType<CameraAnimation>();
        CameraController camControl = camAnim.gameObject.GetComponent<CameraController>();
        Animator playerAnimator = player.GetComponent<Animator>();

        //Remove GUI
        FindObjectOfType<UIGameManager>().ShowGUI(false);

        //stop player movement + painting
        //playerMovement.enabled = false;
        playerMovement.acceptUserInput = false;
        playerMovement.moveLeft = false;
        playerMovement.moveRight = false;

        player.GetComponentInChildren<TouchPaint>().enabled = false;

        //Start celebratory particles, sound and Kiddo-animation, and make Kiddo look out towards the camera
        endLevelParticles.transform.parent = null;
        endLevelParticles.SetActive(true);
        endLevelParticles.GetComponentInChildren<ParticleSystem>().Play();
        AudioPlayer.PlayEvent("EndLevelFeedback", player, 3f);
        actionAnimator.TriggerCelebrationAnim();
        player.transform.LookAt(player.transform.position + Vector3.back);

        //while kiddo is still celebrating, wait
        while (!actionAnimator.HasFinishedAnimation("celebration")) {
            yield return new WaitForEndOfFrame();
        }
        //then go to idle animation
        playerAnimator.Play("idle", 0);

        //Let the celebratory end particles die out
        endLevelParticles.GetComponentInChildren<ParticleSystem>().Stop();
        yield return new WaitForSeconds(2f);

        GameObject focusPoint;
        //Save where to start the pan from
        if (startPanPos == null) {
            focusPoint = Instantiate(new GameObject(), null);
            focusPoint.transform.position = player.transform.position;
            camControl._world = focusPoint;
            camControl._player = focusPoint;
        }
        else {
            focusPoint = startPanPos;
        }

        //Make Kiddo walk through door
        playerMovement.acceptUserInput = true;
        playerMovement.walkThroughDoor = true;
        //playerMovement.enabled = true;
        playerAnimator.Play("run_cleaned", 0);
        while (player.transform.position.z < 7f) {
            yield return new WaitForEndOfFrame();
        }
        playerMovement.enabled = false;
        player.GetComponentInChildren<Renderer>().enabled = false;

        //Close door
        GetComponent<Animator>().Play("DoorDeactivate");
        yield return new WaitForSeconds(0.8f);
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorCloses", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);

        //start panning out
        GameObject endPos = GameObject.FindWithTag("LevelExitPanPos");
        Transform startPos = focusPoint.transform;

        camAnim.Pan(startPos, endPos.transform, 0f, 0f, false);

        //while panning, wait
        while (camAnim.isPanning) {
            yield return new WaitForEndOfFrame();
        }
        camAnim.gameObject.GetComponent<CameraController>()._world = endPos;
        camAnim.gameObject.GetComponent<CameraController>()._player = endPos;

        //Wait a couple of seconds before going to the next scene
        yield return new WaitForSeconds(1f);
        camAnim.BeginFade(0f, 1f, 0.01f);

        while (camAnim.isFading)
            yield return new WaitForEndOfFrame();

        FindObjectOfType<WwiseMusicController>().OnDestroy();

        GameManager.CurrentGameState = GameManager.GameState.End;
    }

    private int GetCurrentLevelNumber() {
        string currentSceneName = SceneManager.GetActiveScene().name;
        currentSceneName = currentSceneName.Substring(5, 1);
        int currentLevel = (int)char.GetNumericValue(currentSceneName[0]);
        if (currentLevel == -1) Debug.Log("I couldn't find a number at 6th position of the level, instead it was: " + currentSceneName);
        return currentLevel;
    }
}
