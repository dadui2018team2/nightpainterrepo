﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InGameDoors : MonoBehaviour {

    Animator animator;
    public Animator lockAnimator;

    public bool exitDoor;

    public bool[] keyParts;
    BoxCollider boxCollider;

    public string sceneToGoTo;

    private bool waitingForPan = false;

    bool hasUnlockedDoor;



    private void Awake() {
        animator = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider>();
        if (!PlayerPrefs.HasKey("CompletedLevels")) {
            PlayerPrefs.SetInt("CompletedLevels", 0);
        }

        hasUnlockedDoor = false;
    }

    private void Start() {
        if (exitDoor) {
            ActivateDoor();


            // ---*** KEEP THIS CODE IN CASE THEY WANT IT CHANGED BACK TO HAVING THE DOORS OPEN AFTER COMPLETING IT ONCE ***---
            /*if(PlayerPrefs.HasKey("CompletedLevels")) {
                int currentLevel = GetSceneValue();
                if (currentLevel <= PlayerPrefs.GetInt("CompletedLevels")) {
                    print("current level: " + currentLevel + ". " + "PlayerPref: " + PlayerPrefs.GetInt("CompletedLevel"));
                    OpenDoor();
                }
            }*/
        }
        else {
            if (MainMenuSettings.previousScene == "MainMenu" || MainMenuSettings.previousScene == "IntroAnimation")
                gameObject.SetActive(false);
            else
                StartCoroutine(DeactivateEntranceDoor());

        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.R)) {
            ResetDoorsPlayerPref();
        }
        if (Input.GetKeyDown(KeyCode.U)) {
            if (exitDoor) {
                UnlockDoor();
            }
        }
        if (Input.GetKeyDown(KeyCode.Space)) {
            PlayOpenSound();
        }
        if (Input.GetKeyDown(KeyCode.M)) {
            AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", gameObject);
            AudioPlayer.PlayEvent("TeleDoor", gameObject, 3f);
        }
    }

    private void ResetDoorsPlayerPref() {
        PlayerPrefs.SetInt("CompletedLevels", 0);
    }

    IEnumerator DeactivateEntranceDoor() {
        CameraAnimation camAnim = FindObjectOfType<CameraAnimation>();
        while (GameManager.CurrentGameState != GameManager.GameState.Play || camAnim.isPanning) {
            yield return new WaitForEndOfFrame();
        }

        animator.Play("DoorDeactivate");
        yield return new WaitForSeconds(1f);
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorCloses", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    public void AddKeyPart(int keyPart) {
        keyParts[keyPart] = true;
        FindObjectOfType<UIGameManager>().UpdatePickupCounter(keyParts);

        if (CheckDoor()) {
            AudioPlayer.LoadBank("TeleDoor");
            AkSoundEngine.PostEvent("DoorIsLockedUp", gameObject);
            AudioPlayer.UnloadBank("TeleDoor", 3f);
        }
    }

    public bool CheckDoor() {
        foreach (bool part in keyParts) {
            if (part == false) return false;
        }
        return true;
    }

    public void UnlockDoor() {
        if (!hasUnlockedDoor) {
            hasUnlockedDoor = true;
            lockAnimator.Play("Unlock");
            Invoke("PlayUnlockSound", 2f);
            Invoke("OpenDoor", 3f);
        }
    }

    public void PlayUnlockSound() {
        AkSoundEngine.PostEvent("KeyUI", gameObject);
    }

    /// <summary>
    /// Activates the door.
    /// </summary>
    public void ActivateDoor() {
        animator.Play("DoorActivate");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Plays activate sound.
    /// </summary>
    public void PlayActivateSound() {
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorAppear", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Opens the door (Should only be called after activating!)
    /// </summary>
    public void OpenDoor() {
        animator.Play("OpenDoor");
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorOpen", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    /// <summary>
    /// Plays open sound.
    /// </summary>
    public void PlayOpenSound() {
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorOpen", gameObject);
        AudioPlayer.PlayEvent("TeleDoor", gameObject, 2f);
    }

    public void PlayCloseSound() {
        AudioPlayer.SetSwitch("TeleDoor", "TeleDoorCloses", gameObject);
        AudioPlayer.PlayEvent("Teledoor", gameObject, 2f);
    }

    /// <summary>
    /// Closes the door.
    /// </summary>
    public void DeactivateDoor() {
        
    }

    private int GetSceneValue() {
        string currentSceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        currentSceneName = currentSceneName.Substring(5, 1);
        int currentLevel = (int)char.GetNumericValue(currentSceneName[0]);
        if (currentLevel == -1) Debug.Log("I couldn't find a number at 6th position of the level, instead it was: " + currentSceneName);
        return currentLevel;
    }

}