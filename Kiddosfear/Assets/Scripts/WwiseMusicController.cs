﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WwiseMusicController : MonoBehaviour {

    private void Awake() {
        AudioPlayer.LoadBank("Loops");
    }

    private void Start() {
        string sceneName = SceneManager.GetActiveScene().name;

        if (sceneName.ToLower().Contains("headphones") || sceneName.ToLower().Contains("mainmenu") || sceneName.ToLower().Contains("level0")) {
            AkSoundEngine.SetState("Music_level", "Menu_Hub");
        }
        else if (sceneName.ToLower().Contains("intro")) {
            print("Changing state to intro");
            AkSoundEngine.SetState("Music_level", "Intro");
        }
        else if (sceneName.ToLower().Contains("level1")) {
            AkSoundEngine.SetState("Music_level", "Level_1");
        }
        else if (sceneName.ToLower().Contains("level2")) {
            AkSoundEngine.SetState("Music_level", "Level_2");
        }
        else if (sceneName.ToLower().Contains("level3")) {
            AkSoundEngine.SetState("Music_level", "Level_3");
        }
        else if (sceneName.ToLower().Contains("level4")) {
            AkSoundEngine.SetState("Music_level", "Level_4");
        }
        else {
            AkSoundEngine.SetState("Music_level", "Menu_Hub");
        }

        AkSoundEngine.SetState("Music_Key", "NoKey");
        AkSoundEngine.SetState("StateOfGame", "InGame");

        if (sceneName.ToLower().Contains("headphones")) {
            PlayMusic();
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Alpha0)) {
            PlayerPrefs.DeleteAll();
        }
    }


    public void OnDestroy() {
        try {
            AkSoundEngine.PostEvent("Stop_Water", FindObjectOfType<SeaCreature>().gameObject);
        }catch(System.NullReferenceException e) {
            Debug.Log(e);
        }
        try {
            AkSoundEngine.PostEvent("Stop_Whale", FindObjectOfType<SeaCreature>().gameObject);
        } catch(System.NullReferenceException e) {
            Debug.Log(e);
        }

        int bankUnload;
        AkSoundEngine.UnloadBank("ActivationOfPaint", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("Animation", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("BossSound", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("ColorPickUps", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("EndLevelFeedback", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("KiddoFootsteps_01", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("KiddoLanding", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("Loops", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("MonsterSound", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("PickUpLight", (System.IntPtr)0, out bankUnload);
        AkSoundEngine.UnloadBank("TeleDoor", (System.IntPtr)0, out bankUnload);
    }


    private void PlayMusic() {
        uint loadBank;
        AkSoundEngine.LoadBank("Music", -1, out loadBank);
        AkSoundEngine.PostEvent("Play_Music", gameObject);
    }

    public void SetMusicState(string stateGroup, string state) {
        AkSoundEngine.SetState(stateGroup, state);
        
    }
}
