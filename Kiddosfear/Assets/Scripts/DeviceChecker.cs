﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeviceChecker : MonoBehaviour {
    //DeviceType 
    //0 = Phone
    //1 = Tablet

    float screenHeight;
    //public Text deviceHeight;
    private void Start()
    {
        float screenWidth = Screen.width / Screen.dpi;
        float screenHeight = Screen.height / Screen.dpi;
        if (screenHeight > 4 && screenHeight < 8 )
        {
            //  deviceHeight.text = "Tablet";
            PlayerPrefs.SetFloat("DeviceType",1);
        }
        else
        {
            // deviceHeight.text = "Phone";
            PlayerPrefs.SetFloat("DeviceType", 0);
        }
    }
}
