﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blobling : MonoBehaviour {

    [SerializeField] Animator meshAnimator;

    [Header("Movement")]
    [Tooltip("If this list is not empty, blobling rotates through the different points. These offsets are in local space, i.e. an offset of (1,0,0) will make the blobling move 1 unit to the right.")]
    [SerializeField] List<Vector3> offsets = new List<Vector3>();
    [SerializeField] float moveSpeed;
    [SerializeField] float pointReachDistance;

    private Vector3 startingPosition;

    private float startSize = 0f;
    private float size = 1f;
    private float sizeSpeed = 2f;

    [Header("Speech")]
    [Tooltip("NOTE: English and Danish text must be set via the LanguageScript component on the speechbubble.")]
    [SerializeField] GameObject speechBubble;
    [SerializeField] Animator speechBubbleAnimator;

    private Transform player;

    private bool playerHasBeenSeen = false;
    private float furthestDistanceFromPlayer;
    private bool shouldMove = false;
    private Vector3 currentPatrolPoint;

    private void Awake() {
        if (offsets.Count > 0) {
            shouldMove = true;
        }

        player = GameObject.FindWithTag("Player").transform;
        startingPosition = transform.position;
        transform.LookAt(transform.position + Vector3.back);
    }

    private void FixedUpdate() {
        if (shouldMove && speechBubbleAnimator.GetCurrentAnimatorStateInfo(0).IsName("Closed")) {
            Move();
        }

        CheckForPlayer();
    }

    private void Move() {
        meshAnimator.SetBool("Move", true);

        // Scaling
        if (transform.localScale.x < size) {
            float newScale = Mathf.Lerp(startSize, size, transform.localScale.x + Time.deltaTime / sizeSpeed);
            transform.localScale = Vector3.one * newScale;
        }

        // Position
        float distanceToPoint = Vector3.Distance(transform.position, startingPosition + offsets[0]);
        Vector3 moveDir = (offsets[0]).normalized;
        if (distanceToPoint < moveSpeed * Time.deltaTime) moveDir *= distanceToPoint;
        else moveDir *= moveSpeed * Time.deltaTime;
        transform.position += moveDir;

        // Rotation
        if (moveDir.magnitude > 0) {
            transform.LookAt(transform.position + offsets[0]);
        }

        // Check destination
        if (distanceToPoint <= pointReachDistance) {
            NextPoint();
        }
    }

    private void NextPoint() {
        offsets.Add(offsets[0]);
        offsets.RemoveAt(0);
        startingPosition = transform.position;
    }

    private void CheckForPlayer() {
        if (playerHasBeenSeen && Vector3.Distance(player.transform.position, transform.position) > furthestDistanceFromPlayer) {
            speechBubbleAnimator.SetBool("Show", false);
            if (offsets.Count > 0)
                shouldMove = true;

            playerHasBeenSeen = false;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && !playerHasBeenSeen) {
            speechBubbleAnimator.SetBool("Show", true);
            transform.LookAt(transform.position + Vector3.back); //looking out into the world towards the camera

            shouldMove = false;
            meshAnimator.SetBool("Move", false);

            meshAnimator.SetTrigger("React");
            AudioPlayer.PlayEvent("MonsterSound", gameObject, 1f);

            playerHasBeenSeen = true;
            furthestDistanceFromPlayer = Vector3.Distance(other.transform.position, transform.position);
        }
    }
}
