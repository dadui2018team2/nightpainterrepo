﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LWCelScript : MonoBehaviour {

	public Material material;
	public float speed;

	private void Start() {
		ResetOffset();
	}

	private void OnDisable() {
		ResetOffset();
	}

	// Update is called once per frame
	private void Update () {
		Vector4 offset = material.GetVector("Vector2_A43D293B");
		offset.x += Time.deltaTime * speed;
		offset.y += Time.deltaTime * speed;
		material.SetVector("Vector2_A43D293B", offset);
	}

	private void ResetOffset(){
		material.SetVector("Vector2_A43D293B", Vector4.zero);
	}
}
